from django.http import HttpResponse, Http404
from django.shortcuts import render

# Create your views here.
from django.views import generic
from exchanger.models import User
from django.template import RequestContext, loader

from .forms import MainForm


def index(request):
    users_list = User.objects.all()
    form = MainForm()
    context = RequestContext(request, {
        'users_list': users_list,
        'form': form
    })
    return render(request, 'exchanger/index.html', context)

def solver(request):
    msg = 'hello'
    if request.method == 'POST':
        form = MainForm(request.POST)
        if form.is_valid():
            selected_user = form.cleaned_data['users_select']
            inns = form.cleaned_data['INNS']
            how_many = form.cleaned_data['how_many']

            try:
                receivers = [x for x in User.objects.all() if x.INN in inns.split(",")]
                how_many_float = float(how_many)
                if selected_user.money > how_many_float:
                    selected_user.money -= how_many_float
                    selected_user.save()
                    for reciver in receivers:
                        reciver.money -= how_many_float / len(receivers)
                        reciver.save()
                msg = 'recs counts:' + len(receivers).__str__()

            except User.DoesNotExist:
                raise Http404("User does not exist")

    context = {'msg': msg}
    return render(request, 'exchanger/solver.html', context)
from django.db import models

# Create your models here.

# user (
#     id,
#     name,
#     INN - может повторяться у разных пользователей,
#     money - в рублях с точностью до копеек)

class User(models.Model):
    uid = models.IntegerField(default=0)
    name = models.CharField(max_length=200)
    INN = models.CharField(max_length=200)
    money = models.FloatField(default=0.0)

    def __str__(self):
        return self.name
__author__ = 'darkerow'

from django import forms

from exchanger.models import User
from django.forms.fields import DateField, ChoiceField, MultipleChoiceField
from django.forms.widgets import RadioSelect, CheckboxSelectMultiple
from django.forms.extras.widgets import SelectDateWidget

class MainForm(forms.Form):
    users_select = forms.ModelChoiceField(queryset=User.objects.all(),
                                         empty_label="Выберите пользователя",
                                         widget=forms.Select(attrs={'class': 'dropdown'}),
                                         label="Пользователи")
    INNS = forms.CharField(label='INNS', max_length=100)
    how_many = forms.CharField(label='How many', max_length=100)
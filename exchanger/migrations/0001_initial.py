# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('uid', models.IntegerField(default=0)),
                ('name', models.CharField(max_length=200)),
                ('INN', models.CharField(max_length=200)),
                ('money', models.FloatField(default=0.0)),
            ],
        ),
    ]

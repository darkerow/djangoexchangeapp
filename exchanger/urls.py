__author__ = 'darkerow'


from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    # ex: /polls/5/vote/
    url(r'^solver/$', views.solver, name='solver'),
]